<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use bitco\usersModels\widgets\LoginWidget;
use bitco\exchange\widgets\DefaultExchangeWidget;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans' type='text/css' media='all' />
    <link rel="shortcut icon" href="favicon.png" type="image/png" />
    <link rel="icon" href="favicon.png" type="image/png" />
    <link rel="icon" href="images/crop-favicon/32x32.png" sizes="32x32" />
    <link rel="icon" href="images/crop-favicon/192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="images/crop-favicon/180x180.png" />
    <meta name="msapplication-TileImage" content="images/crop-favicon/270x270.png" />

    <link rel='canonical' href='/' />

    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <style type="text/css" id="custom-background-css">
        body.custom-background { background-color: #3bf9c4; background-image: url("/images/green-gobbler.png"); background-position: left top; background-size: auto; background-repeat: repeat; background-attachment: scroll; }
    </style>
    <?php $this->head() ?>
</head>
<body class="custom-background" >
<?php $this->beginBody(); ?>
<div id="container">

    <!-- top bar -->
    <div class="topbar_wrap" id="fix_div">
        <div class="topbar_ins" id="fix_elem">
            <div class="topbar">
                <div class="topbar_email">
                    <a href="mailto:supports@bit.co">supports@bit.co</a>
                </div>
                <div class="topbar_phone">
                    <img src="images/icons-s.png"/> <span>(000)-000-00-00 (Предпочтительнее писать в мессенджер)</span>
                </div>

                <?php
                if (Yii::$app->user->isGuest) { ?>
                    <a href="signup" class="toplink">Регистрация</a>
                    <a href="login" class="toplink">Авторизация</a>
                <?php } else {
                    echo Html::a(
                                    'Logout',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'toplink']
                                ) . '<a class="userlogin" href="/cabinet/"><i class="fa fa-user" aria-hidden="true"></i> Кабинет (' . Yii::$app->user->identity->username . ')</a>';
                }
                ?>

                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!-- end top bar -->

    <!-- top menu -->
    <div class="tophead_wrap" >
        <div class="tophead_ins" >
            <div class="tophead">
                <div class="logoblock">
                    <div class="logoblock_ins">
                        <a href="<?php echo Yii::$app->homeUrl; ?>"><img src="/" alt="" /><?php echo Yii::$app->name; ?></a>
                    </div>
                </div>
                <div class="topmenu">
                    <ul id="menu-verhnee-menyu" class="tmenu js_menu">
                        <li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item  first_menu_li menu-item-30"><a href="default.htm"><span>Главная</span></a></li>
                        <li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="news/default.htm"><span>Новости</span></a></li>
                        <li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="reviews/default.htm"><span>Отзывы</span></a></li>
                        <li id="menu-item-193" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193"><a href="programma-loyalnosti/default.htm"><span>Программа лояльности</span></a></li>
                        <li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="tarifs/default.htm"><span>Тарифы</span></a></li>
                        <li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page  last_menu_li menu-item-31"><a href="site/contact"><span>Контакты</span></a></li>
                    </ul>

                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['site/index']],
	['label' => 'Exchange', 'url' => ['exchange/exchange/index']],
        ['label' => 'Contact', 'url' => ['site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['auth/signup/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    <!-- end top menu -->
    <div class="wrapper">
        <div class="contentwrap">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        <div class="sidebar">
            <?php
            if (Yii::$app->user->isGuest) {
                echo LoginWidget::widget();
            }
            ?>
        </div>
        <div class="clear"></div>

        </div>
    </div>



    <!-- footer -->
    <div class="footer_wrap">
        <div class="footer">
            <div class="footer_left">
                <div class="copyright">
                    Сервис обмена электронных валют.
                    <div style="margin:20px 0 0 0;">
                        <a href="../https@www.bestchange.ru/default.htm">
                            <img src="/images/bestchange.gif" title="Обмен Perfect Money, BTC-e, OKPay, Bitcoin" alt="Мониторинг обменных пунктов BestChange.ru" width="88" height="31" border="0" /></a>
                    </div>
                </div>
                <div class="footer_menu">
                    <div class="clear"></div>
                </div>
            </div>
            <div class="footer_center">

                 <a title="Купить bitcoin за наличные гривны" href="/exchange/uah-to-btc">Купить bitcoin за наличные гривны</a>
                <br /><a title="Купить bitcoin за наличные доллары" href="/exchange/usd-to-btc">Купить bitcoin за наличные доллары</a>
                <br /><a title="Продать bitcoin за наличные гривны" href="/exchange/btc-to-uah">Продать bitcoin за наличные гривны</a>
                <br /><a title="Продать bitcoin за наличные доллары" href="/exchange/btc-to-usd">Продать bitcoin за наличные доллары</a>

                <div class="clear"></div>

            </div>

            <div class="footer_right">
                <div class="footer_timetable">
                    <p>Пн. — Пт. с 10:00 до 17:00.<br /> Сб. — Вс. свободный график.</p>

                    <br/><div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="default.htm" itemprop="url"><span itemprop="title">Обменник криптовалют</span></a> › </div><div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#goodchoice" itemprop="url"><span itemprop="title">Продать, вывести, обналичить биткоины</span></a></div></div>
            </div>

        </div>
        <p class="pull-left">&copy; <?= date('Y') ?></p>
        <div class="clear"></div>
    </div>
    <!-- end footer -->
</div>

<div id="topped"></div>
<div id="the_shadow" class="js_techwindow"></div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
