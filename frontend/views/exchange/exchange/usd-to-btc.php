<?php

use yii\helpers\Html;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use bitco\exchange\entities\SignsCurrency;
use bitco\exchange\entities\DirectionsOfExchange;
use bitco\exchange\forms\ExchangeForm;
use bitco\exchange\forms\DeliveryForm;

$dataDirection = DirectionsOfExchange::find()->
select(['id', 'in_signs', 'in_unit', 'out_signs', 'default_unit', 'level_exchange', 'slug', 'status'])->asArray()->all();
$dataSings = SignsCurrency::find()->
select(['id', 'full_name_currency', 'short_name_currency', 'href_icon'])->asArray()->all();
$one = \yii\helpers\Json::htmlEncode($dataDirection);
$two = \yii\helpers\Json::htmlEncode($dataSings);
$JS = <<< JS
    var dataDirection = $one;
    var dataSings = $two;
JS;
$this->registerJS($JS, View::POS_END);
$this->registerJsFile('js/form.js',
    ['position' => yii\web\View::POS_END,
        'depends' => 'yii\web\YiiAsset']);

$this->title = 'Herre!!';
?>
<div class="contentwrap">
<div class="thecontent">
    <div class="notice_message">
        <div class="notice_message_ins">
            <div class="notice_message_abs"></div>
            <div class="notice_message_close"></div>
            <div class="notice_message_title">
                <div class="notice_message_title_ins">
                    <span>Внимание!</span>
                </div>
            </div>
            <div class="notice_message_text">
                <div class="notice_message_text_ins">
                    <p>Обращаем внимание, что BTC после отправки будут зачислены вам на счет моментально, но использовать вы их сможете после 3-го подтверждения в системе Bitcoin.</p>

                </div>
            </div>
        </div>
    </div>
    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <div class="xchange_div">
        <div class="xchange_div_ins">
            <div class="xchange_data_title otd">
                <div class="xchange_data_title_ins">
                    <span>Отдаете</span>
                </div>
            </div>
            <div class="xchange_data_div">
                <div class="xchange_data_ins">
                    <div class="xchange_data_left">

                    </div>
                    <div class="xchange_data_right">

                    </div>
                    <div class="clear"></div>
                    <div class="xchange_data_left">
                        <?php
                        echo $form->field($model->exchange, 'signs_in')->dropdownList(
                            SignsCurrency::find()->select(['full_name_currency', 'id'])->indexBy('id')->column()
                        );
                        ?>
                    </div>
                    <div class="xchange_data_right">
                        <?= $form->field($model->exchange, 'value_in')->textInput(); ?>
                    </div>
                    <div class="clear"></div>
                    <div class="xchange_data_left">

                        <div class="xchange_sumandcom" style="display: none;">
                            <span class="js_comis_text1"></span>
                        </div>
                    </div>
                    <div class="xchange_data_right">
                        <div class="xchange_sum_line" style="display: none;">
                            <div class="xchange_sum_label">
                                Сумма<span class="red">*</span>:
                            </div>

                            <div class="xchange_sum_input js_wrap_error js_wrap_error_br ">
                                <input type="text" name="" class="js_summ1c" value="1">
                                <div class="js_error js_summ1c_error"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="xchange_data_left">




                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="xchange_data_title pol">
                <div class="xchange_data_title_ins">
                    <span>Получаете</span>
                </div>
            </div>
            <div class="xchange_data_div">
                <div class="xchange_data_ins">
                    <div class="xchange_data_left">
                        <div class="xchange_info_line"></div>
                    </div>
                    <div class="xchange_data_right">
                        <div class="xchange_info_line"><span class="span_get_max">min.: 270000 UAH, max.: 600000 UAH</span></div>
                    </div>
                    <div class="clear"></div>
                    <div class="xchange_data_left">
                        <?php
                        echo $form->field($model->exchange, 'signs_out')->dropdownList(
                            SignsCurrency::find()->select(['full_name_currency', 'id'])->indexBy('id')->column()
                        );
                        ?>
                    </div>
                    <div class="xchange_data_right">
                        <?= $form->field($model->exchange, 'value_out')->textInput() ?>
                    </div>
                    <div class="clear"></div>

                    <div class="xchange_data_left">

                        <div class="xchange_curs_line">
                            <div class="xchange_curs_line_ins">
                                <div class="xchange_curs_label">
                                    <div class="xchange_curs_label_ins">
                                        <label for="account2"><span class="xchange_label">На кошелек<span class="req">*</span>: <span class="help_tooltip_label"></span></span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="xchange_curs_input js_wrap_error js_wrap_error_br js_window_wrap">
                                <?= $form->field($model->delivery, 'purse')->textInput() ?>

                            </div>
                            <div class="clear"></div>

                        </div>


                    </div>
                    <div class="clear"></div>

                </div>
            </div>


            <div class="xchange_pers">
                <div class="xchange_pers_ins">

                    <div class="xchange_pers_title">
                        <div class="xchange_pers_title_ins">
                            <span>Личные данные</span>
                        </div>
                    </div>
                    <div class="xchange_pers_div">
                        <div class="xchange_pers_div_ins">
                            <div class="xchange_pers_line has_help">

                                <div class="xchange_pers_input">
                                    <?= $form->field($model->delivery, 'surname')->textInput() ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="xchange_pers_line has_help">

                                <div class="xchange_pers_input">
                                    <?= $form->field($model->delivery, 'name')->textInput() ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="xchange_pers_line has_help">

                                <div class="xchange_pers_input">
                                    <?= $form->field($model->delivery, 'email')->input('email'); ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="xchange_pers_line has_help">

                                <div class="xchange_pers_input">
                                    <?= $form->field($model->delivery, 'number_phone')->textInput() ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="xchange_submit_div">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <div class="clear"></div>
            </div>

            <div class="xchange_checkdata_div">
                <div class="checkbox "><input type="checkbox" id="check_data" name="check_data" value="1"> Запомнить введенные данные</div>
            </div>


            <div class="ajax_post_bids_res"></div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
</div>
	