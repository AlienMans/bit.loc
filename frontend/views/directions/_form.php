<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DirectionsOfExchange */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="directions-of-exchange-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'in_signs')->textInput() ?>

    <?= $form->field($model, 'in_unit')->textInput() ?>

    <?= $form->field($model, 'out_signs')->textInput() ?>

    <?= $form->field($model, 'out_unit')->textInput() ?>

    <?= $form->field($model, 'level_exchange')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
