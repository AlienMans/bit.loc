<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DirectionsOfExchangeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Directions Of Exchanges';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directions-of-exchange-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Directions Of Exchange', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'in_signs',
            'in_unit',
            'out_signs',
            'out_unit',
            // 'level_exchange',
            // 'slug',
            // 'status',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
