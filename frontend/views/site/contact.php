<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">

    <div class="thecontent">
	<div class="text">
		<p>Наш обменный пункт находится в Киеве</p>
		<p>Для связи с нами используйте наиболее удобные для вас контакты:</p>
		<p><strong>E-mail — support@bitco</strong></p>
		<p>Телефон для Viber, Whatsapp, Telegram:<br>
		(000)-000-00-00 — Киевстар (Предпочтительнее писать в мессенджер)</p>
		<p>Вы можете также связаться с нами через чат в нижнем правом углу на нашем сайте.</p>
		<div class="clear"></div>
	</div>
   </div>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'subject') ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
