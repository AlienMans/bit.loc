<?php
namespace frontend\controllers\exchange;

use bitco\exchange\entities\DataExchange;
use bitco\exchange\entities\DirectionsOfExchange;
use bitco\exchange\manage\DataExchangeManage;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use bitco\exchange\entities\SignsCurrency;
use bitco\exchange\forms\ExchangeCreateForm;


/**
 * Exchange controller
 */
class ExchangeController extends Controller
{

    /**
     * @var DataExchangeManage
     */
    private $service;

    public function __construct($id, $module, DataExchangeManage $service, $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'mymain';

        return $this->render('index' );
    }

    /**
     * @param $slug
     * @return string|\yii\web\Response
     */
    public function actionExchange($slug)
    {
        var_dump(Yii::$app->request->userIP);
        $cookies = Yii::$app->response->cookies;
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'dfsd',
            'value' => 'swdwd'
        ]));

        var_dump(Yii::$app->response->cookies->getValue('dfsd'));



        $this->layout = 'mymain-exch';
        $view = $this->service->getCurrentExchangeView($slug);
        if ($view == '') {
            Yii::$app->session->setFlash('error', 'Такого направления не существует');
            return $this->redirect('/exchange');
        }

        $form = new ExchangeCreateForm();
        $form->exchange->loadDefaults(
                $slug,
                ArrayHelper::getValue(Yii::$app->request->post(), 'param1'),
                ArrayHelper::getValue(Yii::$app->request->post(), 'param2'));

        if(!Yii::$app->user->isGuest) {
            $form->delivery->loadUser(Yii::$app->user->identity->getId());
        }

        if (Yii::$app->request->post('DeliveryForm') && $form->validate()) {

            $this->service->create(Yii::$app->request->post('ExchangeForm'), Yii::$app->request->post('DeliveryForm'));
            Yii::$app->session->setFlash('success', 'Спасибо, Ваша заявка отправлена');
            return $this->redirect('/exchange');
        }
            return $this->render($view, [
            'model' => $form,
        ]);


    }

}
