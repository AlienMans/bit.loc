<?php
namespace frontend\controllers\auth;


use bitco\usersModels\IdentityUsers;
use Yii;
use yii\web\Controller;
use bitco\usersModels\forms\auth\LoginForm;
use bitco\usersModels\services\AuthService;


class AuthController extends Controller
{

    private $service;

    public function __construct($id, $module, AuthService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $form = new LoginForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $user = $this->service->auth($form);
                Yii::$app->user->login(new IdentityUsers($user), $form->rememberMe ? Yii::$app->params['user.rememberMeDuration'] : 0);
                return $this->redirect(['cabinet/default/index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('login', [
            'model' => $form,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
