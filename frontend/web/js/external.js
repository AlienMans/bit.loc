function getDirectionActive(in_signs, out_signs) {
    for (var i = 0; i < dataDirection.length; i++) {
        if (dataDirection[i].in_signs == in_signs && dataDirection[i].out_signs == out_signs)
        {
            return dataDirection[i].status == 10 ? true : false;
        }
    }};
function getShortName(id) {
    return dataSings[id].short_name_currency;
}
function changeInfoSigns(in_signs, out_signs){
    for (var i = 0; i < dataDirection.length; i++) {
        if (dataDirection[i].in_signs == in_signs && dataDirection[i].out_signs == out_signs)
        {
            st = dataDirection[i].default_unit + ' ' + getShortName(dataDirection[i].in_signs - 1);
            calc = +((dataDirection[i].default_unit * dataDirection[i].level_exchange) * 100 / 100).toFixed(10);
            st += ' = ' + calc.toString().replace(/.0+$/,'') + ' ' + getShortName(dataDirection[i].out_signs - 1);
            str = $( 'span.js_curs_html' ).text(st);
            return true;
        }
    }};
function Calculate(in_data, in_signs, out_signs, rev) {
    for (var i = 0; i < dataDirection.length; i++) {
        if (dataDirection[i].in_signs == in_signs && dataDirection[i].out_signs == out_signs)
        {
            if(!rev) {
                calc = (in_data * dataDirection[i].level_exchange * 100) / 100;
            } else { calc = ( (in_data * 100) * ((dataDirection[i].in_unit * 100) / (dataDirection[i].level_exchange * 100) ) / 100); }
            if(isNaN(calc)) {
                return 'Введите число';
            }
            return calc;
        }
    }};
function getSlug(in_signs, out_signs) {
    for (var i = 0; i < dataDirection.length; i++) {
        if (dataDirection[i].in_signs == in_signs && dataDirection[i].out_signs == out_signs)
        {
            return dataDirection[i].slug;
        }
    }};
var in_sum = $('#in_sum');
var out_sum = $('#out_sum');
function changeCalculate(data_in, data_out, rev = null) {
    data_in.keyup(function () {
        sum = Calculate(data_in.val(), $( '#js_left_sel option:selected' ).val(), $( '#js_right_sel option:selected' ).val(), rev );
        data_out.val(sum);

        $( '#xtl_submit_wrap a' ).attr("data-params",
            '{"param1":"'+ in_sum.val() + '","param2":"'+ out_sum.val() + '"}');
    });
}
$( '#js_left_sel' ).change(function () {
	$( '#js_right_sel' ).find( 'option:not(:first)' ).end().prop( 'disabled',true );
	var select_val = $( this ).val();
	$('#js_right_sel option').each(function
		() {
			this.remove();
		});
	var count = 0;
	for (var i = 0; i < dataSings.length; i++) {
		if(dataSings[i].id == select_val || !getDirectionActive(select_val, dataSings[i].id, status))
		{ dis = ' disabled="disabled"'; count++;
		} else { dis = '';};

        $( '#js_right_sel' ).
        append( '<option value="' + dataSings[i].id + '"' + dis + '>' + dataSings[i].full_name_currency + '</option>' );

    };
	if(count == dataSings.length) {
		$('#js_right_sel').
		prepend('<option value="0" selected="selected">Недоступен обмен</option>');
	};
    $( '#xtl_submit_wrap a' ).attr("href", 'exchange/' +
    getSlug($( '#js_left_sel option:selected' ).val(), $( '#js_right_sel option:selected' ).val()));
	$( '#js_right_sel' ).prop( 'disabled', false );
    sum = Calculate(in_sum.val(), $( '#js_left_sel option:selected' ).val(), $( '#js_right_sel option:selected' ).val() );
    out_sum.val(sum);
    changeInfoSigns($( '#js_left_sel option:selected' ).val(), $( '#js_right_sel option:selected' ).val());

    $( '#xtl_submit_wrap a' ).attr("data-params",
        '{"param1":"'+ in_sum.val() + '","param2":"'+ out_sum.val() + '"}');
});

$( '#js_right_sel' ).change(function () {
    sum = Calculate(in_sum.val(), $( '#js_left_sel option:selected' ).val(), $( '#js_right_sel option:selected' ).val() );
    out_sum.val(sum);
    changeInfoSigns($( '#js_left_sel option:selected' ).val(), $( '#js_right_sel option:selected' ).val());
    $( '#xtl_submit_wrap a' ).attr("href", 'exchange/' +
        getSlug($( '#js_left_sel option:selected' ).val(), $( '#js_right_sel option:selected' ).val()));
});
changeCalculate(in_sum, out_sum);
changeCalculate(out_sum, in_sum, true);