<?php
return array(
    'adminEmail' => 'admin@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'user.rememberMeDuration' => 3600 * 24 * 30,
);
