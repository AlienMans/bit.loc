<?php

namespace bitco\exchange\widgets;

use yii;
use yii\base\Widget;
use yii\helpers\Html;
use bitco\exchange\entities\DirectionsOfExchange;
use bitco\exchange\entities\SignsCurrency;

class DefaultExchangeWidget extends Widget
{


    public function run()
    {
        $dataDirection = DirectionsOfExchange::find()->
        select(['id', 'in_signs', 'in_unit', 'out_signs', 'default_unit', 'level_exchange', 'slug', 'status'])->asArray()->all();
        $dataSings = SignsCurrency::find()->
        select(['id', 'full_name_currency', 'short_name_currency', 'href_icon'])->asArray()->all();


        return $this->render('index', [
            'dataDirection' => $dataDirection,
            'dataSings' => $dataSings
        ]);
    }

}