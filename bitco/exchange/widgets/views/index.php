<?php
/**
 * Created by PhpStorm.
 * User: AlienMans
 * Date: 03.01.2018
 * Time: 9:54
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use bitco\exchange\entities\DirectionsOfExchange;
use bitco\exchange\entities\SignsCurrency;
use bitco\siteOptions\entities\SiteOptions;

$one = \yii\helpers\Json::htmlEncode($dataDirection);
$two = \yii\helpers\Json::htmlEncode($dataSings);
$JS = <<< JS
    var dataDirection = $one;
    var dataSings = $two;
JS;
$this->registerJS($JS, View::POS_END);
$this->registerJsFile('js/external.js',
    ['position' => yii\web\View::POS_END,
        'depends' => 'yii\web\YiiAsset']);

?>
<div class="xchange_table_wrap">

    <div class="xchange_type_list">
        <div class="xchange_type_list_ins">

            <div class="xtl_table_wrap">
                <div class="xtl_table_wrap_ins">
                    <div class="xtl_table_top">

                        <div class="xtl_left_col">
                            <div class="xtl_table_title">
                                <div class="xtl_table_title_ins">
                                    Вы отдаете
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="xtl_right_col">
                            <div class="xtl_table_title">
                                <div class="xtl_table_title_ins">
                                    Вы получаете
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>

                    </div>
                    <div class="xtp_html_wrap">

                        <div id="js_html">

                            <div class="xtl_table_body_wrap">

                                <div class="xtl_left_col">

                                    <div class="xtl_ico_wrap">
                                        <div class="xtl_ico" style="background: url(/images/Bitcoin.png) no-repeat center center;"></div>
                                    </div>

                                    <div class="xtl_select_wrap">
                                        <select name="" id="js_left_sel" class="js_my_sel" autocomplete="off">
                                            <?php
                                            echo SignsCurrency::getLeftFormOptions();
                                            ?>
                                        </select>
                                    </div>

                                </div>
                                <div class="xtl_center_col">
                                    <a href="#" class="xtl_change" id="js_reload_table"></a>
                                </div>
                                <div class="xtl_right_col">

                                    <div class="xtl_ico_wrap">
                                        <div class="xtl_ico" style="background: url(/images/cash.png) no-repeat center center;"></div>
                                    </div>

                                    <div class="xtl_select_wrap">
                                        <select name="" id="js_right_sel" class="js_my_sel" autocomplete="off">
                                            <?php
                                            echo SignsCurrency::getRightFormOptions();
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <input type="hidden" name="" class="js_napr_id" value="17" />
                            <div class="xtl_table_body">

                                <div class="xtl_left_col">
                                    <div class="xtl_input_wrap js_wrap_error js_wrap_error_br ">
                                        <input type="text" name="sum1"  id="in_sum" class="js_summ1 cache_data"
                                               value="<?php
                                               echo DirectionsOfExchange::getDefaultDirection()->default_unit;
                                               ?>" />
                                        <div class="js_error js_summ1_error"></div>
                                    </div>
                                </div>
                                <div class="xtl_right_col">
                                    <div class="xtl_input_wrap js_wrap_error js_wrap_error_br ">
                                        <input type="text" name=""  id="out_sum" class="js_summ2"
                                               value="<?php
                                               $value = DirectionsOfExchange::getDefaultDirection()->default_unit * DirectionsOfExchange::getDefaultDirection()->level_exchange;
                                               echo rtrim(rtrim(sprintf("%.15lf", $value), "0"), ".");
                                               ?>" />
                                        <div class="js_error js_summ2_error"></div>
                                    </div>
                                    <div class="xtl_line xtl_exchange_rate">
                                        Курс обмена: <br /><span class="js_curs_html"><?php
                                            echo SignsCurrency::getSignsExchange(); ?></span>
                                    </div>

                                </div>
                                <div class="clear"></div>
                            </div>
                            <div id="xtl_submit_wrap">
                                <?php
                                echo Html::a('Обменять', [
                                    'exchange/'. DirectionsOfExchange::getDefaultDirection()->slug
                                ],
                                    [
                                        'class' => 'xtl_submit js_exchange_link ',
                                        'id' => 'js_submit_button',
                                        'data' => [
                                            'method' => 'post',
                                            'params' => [
                                                'param1' => DirectionsOfExchange::getDefaultDirection()->default_unit,
                                                'param2' => rtrim(rtrim(sprintf("%.15lf", $value), "0"), "."),
                                            ],
                                        ],
                                    ]);
                                ?>
                                <div class="clear"></div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>