<?php

namespace bitco\exchange\helpers;

use bitco\exchange\entities\DataExchange;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class DataExchangeStatusHelper
{
    public static function statusList()
    {
        return [
            DataExchange::STATUS_ACTIVE => 'Active',
            DataExchange::STATUS_PENDING => 'Pending',
            DataExchange::STATUS_CLOSED => 'Closed',
        ];
    }

    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case DataExchange::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            case DataExchange::STATUS_PENDING:
                $class = 'label label-warning';
                break;
            case DataExchange::STATUS_CLOSED:
                $class = 'label label-danger';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}