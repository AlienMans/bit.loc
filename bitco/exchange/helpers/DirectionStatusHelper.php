<?php

namespace bitco\exchange\helpers;

use bitco\exchange\entities\DirectionsOfExchange;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class DirectionStatusHelper
{
    public static function statusList()
    {
        return [
            DirectionsOfExchange::STATUS_ACTIVE => 'Active',
            DirectionsOfExchange::STATUS_DISABLED => 'Disabled',
        ];
    }

    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case DirectionsOfExchange::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            case DirectionsOfExchange::STATUS_DISABLED:
                $class = 'label label-danger';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}