<?php

namespace bitco\exchange\manage;

use Yii;
use bitco\exchange\entities\DataExchange;
use bitco\exchange\entities\DirectionsOfExchange;
use bitco\exchange\repositories\DataExchangeRepository;
use bitco\exchange\forms;

class DataExchangeManage

{
    private $data;

    public function __construct(DataExchangeRepository $data)
    {
        $this->data = $data;
    }

    public function getCurrentExchangeView($slug){

        $direction = DirectionsOfExchange::getDirection($slug);
        if ($direction && $direction->isActive()) {
            $nameController = Yii::$app->controller->action->id;
            $viewCurrent = Yii::getAlias('@frontend').'/views/'.
                $nameController . '/' . $nameController .'/'. $direction->slug .'.php';
            if ( file_exists($viewCurrent) ) {
                $view = $direction->slug;
            } else { $view = 'create'; }

            return $view;
        }
        return '';
    }

    public function create($exf, $delf)
    {

        $this->data = new DataExchange();
        $this->data->exchange_direction = DirectionsOfExchange::getId($exf['signs_in'], $exf['signs_out']);
        $this->data->incoming_amount = $exf['value_in'];
        $this->data->outgoing_amount = '' . $exf['value_in'] * $this->data->exchangeDirection->level_exchange;
        $this->data->level_exchange = '' . $this->data->exchangeDirection->level_exchange;
        $this->data->real_outgoing_amount = $exf['value_out'];
        $this->data->real_level_exchange = '' . $this->data->exchangeDirection->level_exchange;
        if(!Yii::$app->user->isGuest) {
            $this->data->user_id = Yii::$app->user->identity->getId();
        } else { $this->data->user_id = null; }
        $this->data->user_fullname = $delf['surname'];
        $this->data->user_name = $delf['name'];
        $this->data->user_email = $delf['email'];
        $this->data->user_phone = $delf['number_phone'];
        $this->data->user_purse = $delf['purse'] ?: '';
        $this->data->status = 20;
        $this->data->admin_user_id = 1;
        $this->data->save();
        return $this->data;
    }

}