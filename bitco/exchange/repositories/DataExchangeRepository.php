<?php

namespace bitco\exchange\repositories;

use bitco\exchange\entities\DataExchange;


class DataExchangeRepository {

    public function get($id)
    {
        if (!$data = DataExchange::findOne($id)) {
            throw new \DomainException('DataExchange is not found.');
        }
        return $data;
    }

    public function save(DataExchange $data)
    {
        if (!$data->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(DataExchange $data)
    {
        if (!$data->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
