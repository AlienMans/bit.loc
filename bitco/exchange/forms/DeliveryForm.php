<?php

namespace bitco\exchange\forms;

use bitco\exchange\entities\Delivery;
use bitco\usersModels\entities\User;
use yii\base\Model;

class DeliveryForm extends Model
{
    public $purse;
    public $surname;
    public $name;
    public $email;
    public $number_phone;

    public function __construct(Delivery $delivery = null, array $config = [])
    {
        if($delivery) {
            $this->purse = $delivery->purse;
            $this->surname = $delivery->surname;
            $this->name = $delivery->name;
            $this->email = $delivery->email;
            $this->number_phone = $delivery->number_phone;
        }
        parent::__construct($config);
    }

    public  function loadUser($user_id) {

        $user = User::findOne(['id' => $user_id]);
        $this->purse = '';
        $this->surname = $user->surname;
        $this->name = $user->name;
        $this->email = $user->email;
        $this->number_phone = $user->number_phone;
        return $user;
    }

    public function rules()
    {
        return [
            [['surname', 'name', 'email'], 'required', 'message' => 'Введите персональные данные'],
            [['email'], 'email', 'message' => 'Введите email'],
            [['number_phone'], 'required', 'message' => 'Введите номер телефона'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'purse' => 'Кошелек',
            'surname' => 'Ваша фамилия',
            'name' => 'Ваше имя',
            'email' => 'Ваш email',
            'number_phone' => 'Номер телефона'
        ];
    }
}