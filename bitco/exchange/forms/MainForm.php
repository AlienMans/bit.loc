<?php

namespace bitco\exchange\forms;

use bitco\exchange\entities\Exchange;
use yii\base\Model;

class MainForm extends Model
{
    public $signs_in;
    public $signs_out;
    public $value_in;
    public $value_out;

    public function __construct(Exchange $exchange = null, array $config = [])
    {
        if($exchange) {
            $this->signs_in = $exchange->signs_in;
            $this->signs_out = $exchange->signs_out;
            $this->value_in = $exchange->value_in;
            $this->value_out = $exchange->value_out;
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['value_in', 'value_out'], 'required', 'message' => 'Введите сумму для обмена.'],
            [['value_in', 'value_out'], 'double', 'message' => 'Введите сумму для обмена.' ]
        ];
    }
}