<?php

namespace bitco\exchange\forms;

use bitco\exchange\CompositeForm;
use bitco\exchange\forms\ExchangeForm;
use bitco\exchange\forms\DeliveryForm;
use yii\base\Model;


/**
 * @property DeliveryForm $delivery
 * @property ExchangeForm $exchange
*/

class ExchangeCreateForm extends Model
{

    public $exchange;
    public $delivery;

    public function __construct(array $config = [])
    {
        $this->exchange = new ExchangeForm();
        $this->delivery = new DeliveryForm();

        parent::__construct($config);
    }

    protected function internalForms()
    {
        return ['exchange', 'delivery'];
    }

}