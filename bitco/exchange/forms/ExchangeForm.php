<?php

namespace bitco\exchange\forms;

use bitco\exchange\entities\DirectionsOfExchange;
use bitco\exchange\entities\Exchange;
use yii\base\Model;

class ExchangeForm extends Model
{
    public $signs_in;
    public $signs_out;
    public $value_in;
    public $value_out;

    public function __construct(Exchange $exchange = null, array $config = [])
    {
        if($exchange) {
            $this->signs_in = $exchange->signs_in;
            $this->signs_out = $exchange->signs_out;
            $this->value_in = $exchange->value_in;
            $this->value_out = $exchange->value_out;
        }
        parent::__construct($config);
    }

    public function loadDefaults ($slug, $value_in, $value_out)
    {
        $direction = DirectionsOfExchange::getDirection($slug);
        if($direction) {
            $this->signs_in = $direction->in_signs;
            $this->signs_out = $direction->out_signs;
        }
        $this->value_in = $value_in ?: $direction->default_unit;
        $this->value_out = $value_out ?: $direction->level_exchange * $direction->default_unit;

    }

    public function rules()
    {
        return [
            [['value_in', 'value_out'], 'required', 'message' => 'Введите сумму для обмена.'],
            [['value_in', 'value_out'], 'double', 'message' => 'Введите сумму для обмена.' ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'signs_in' => '',
            'signs_out' => '',
            'value_in' => 'Сумма*',
            'value_out' => 'Сумма*'
        ];
    }
}