<?php

namespace bitco\exchange\entities;

use bitco\siteOptions\entities\SiteOptions;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use bitco\exchange\entities\SignsCurrency;

/**
 * This is the model class for table "directions_of_exchange".
 *
 * @property integer $id
 * @property integer $in_signs
 * @property integer $in_unit
 * @property integer $out_signs
 * @property integer $default_unit
 * @property string $level_exchange
 * @property string $slug
 * @property integer $status
 * @property integer $updated_at
 *
 * @property DataExchange[] $dataExchanges
 * @property SignsCurrency $inSigns
 * @property SignsCurrency $outSigns
 */

class DirectionsOfExchange extends ActiveRecord
{
    const STATUS_ACTIVE = 10;
    const STATUS_DISABLED = 0;

    public static function tableName()
    {
        return 'directions_of_exchange';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['in_signs', 'out_signs', 'level_exchange', 'slug',  'updated_at'], 'required'],
            [['in_signs', 'in_unit', 'out_signs', 'default_unit', 'status', 'updated_at'], 'integer'],
            [['level_exchange'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.,]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
            [['slug'], 'string', 'max' => 255],
            [['in_signs'], 'exist', 'skipOnError' => true, 'targetClass' => SignsCurrency::className(), 'targetAttribute' => ['in_signs' => 'id']],
            [['out_signs'], 'exist', 'skipOnError' => true, 'targetClass' => SignsCurrency::className(), 'targetAttribute' => ['out_signs' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'in_signs' => 'Входная валюта',
            'in_unit' => 'Входное значение',
            'out_signs' => 'Выходная валюта',
            'default_unit' => 'Сумма обмена по умолчанию',
            'level_exchange' => 'Курс валюты',
            'slug' => 'Адрес-слаг валюты',
            'status' => 'Статус направления',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function getDataExchanges()
    {
        return $this->hasMany(DataExchange::className(), ['exchange_direction' => 'id']);
    }

    public function getInSigns()
    {
        return $this->hasOne(SignsCurrency::className(), ['id' => 'in_signs']);
    }

    public function getOutSigns()
    {
        return $this->hasOne(SignsCurrency::className(), ['id' => 'out_signs']);
    }

    // ==== back ==== //
    public function getNamedDirection($id) {
        $str = '';
        $this->findOne($id);
        $str .= $this->in_unit . ' ' . $this->inSigns->short_name_currency. ' = ';
        $str .= Yii::$app->formatter->asText($this->level_exchange) . ' ' . $this->outSigns->short_name_currency;
        return $str;
    }

    public static function getId ($in_signs, $out_signs){

        return self::find()->where(['in_signs' => $in_signs, 'out_signs' => $out_signs])->limit(1)->one()->id;
    }

    // ==== front ==== //
    public static function getDirection($slug){

        return self::find()->where(['slug' => $slug])->one();
    }

    public static function getDefaultDirection()
    {
        return self::findOne(['id' => SiteOptions::getDefaultDirectionOption()]);
    }

    public function isActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isDisabled()
    {
        return $this->status === self::STATUS_DISABLED;
    }

}
