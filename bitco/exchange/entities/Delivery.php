<?php

namespace bitco\exchange\entities;

class Delivery
{
    public $purse;
    public $surname;
    public $name;
    public $email;
    public $number_phone;

    public function __construct($purse, $surname, $name, $email, $number_phone)
    {
        $this->purse = $purse;
        $this->surname = $surname;
        $this->name = $name;
        $this->email= $email;
        $this->number_phone = $number_phone;
    }

}