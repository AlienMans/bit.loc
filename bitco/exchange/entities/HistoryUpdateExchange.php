<?php

namespace bitco\exchange\entities;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "history_update_exchange".
 *
 * @property integer $id
 * @property integer $directions_id
 * @property integer $old_in_unit
 * @property integer $old_out_unit
 * @property string $old_level_exchange
 * @property integer $old_status
 * @property integer $updated_at
 */
class HistoryUpdateExchange extends ActiveRecord
{

    public static function tableName()
    {
        return 'history_update_exchange';
    }

    public function rules()
    {
        return [
            [['directions_id', 'old_in_unit', 'old_out_unit', 'old_level_exchange', 'updated_at'], 'required'],
            [['directions_id', 'old_in_unit', 'old_out_unit', 'old_status', 'updated_at'], 'integer'],
            //[['old_level_exchange'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'directions_id' => 'Directions ID',
            'old_in_unit' => 'Old In Unit',
            'old_out_unit' => 'Old Out Unit',
            'old_level_exchange' => 'Old Level Exchange',
            'old_status' => 'Old Status',
            'updated_at' => 'Updated At',
        ];
    }
}
