<?php

namespace bitco\exchange\entities;

class Exchange
{
    public $signs_in;
    public $signs_out;
    public $value_in;
    public $value_out;

    public function __construct($signs_in, $signs_out, $value_in, $value_out)
    {
        $this->signs_in = $signs_in;
        $this->signs_out = $signs_out;
        $this->value_in = $value_in;
        $this->value_out = $value_out;
    }
}