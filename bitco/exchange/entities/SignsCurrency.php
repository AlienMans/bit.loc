<?php

namespace bitco\exchange\entities;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use bitco\siteOptions\entities\SiteOptions;

/**
 * This is the model class for table "signs_currency".
 *
 * @property integer $id
 * @property string $full_name_currency
 * @property string $short_name_currency
 * @property integer $created_at
 * @property string $href_icon
 *
 * @property DirectionsOfExchange[] $directionsOfExchanges
 * @property DirectionsOfExchange[] $directionsOfExchanges0
 */
class SignsCurrency extends ActiveRecord
{

    public static function tableName()
    {
        return 'signs_currency';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['full_name_currency', 'short_name_currency', 'created_at'], 'required'],
            [['created_at'], 'integer'],
            [['full_name_currency', 'short_name_currency', 'href_icon'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name_currency' => 'Полное название валюты',
            'short_name_currency' => 'Сокращенное название валюты',
            'created_at' => 'Валюта зарегистрирована',
            'href_icon' => 'Изображение валюты',
        ];
    }

    public function getDirectionsOfExchanges()
    {
        return $this->hasMany(DirectionsOfExchange::className(), ['in_signs' => 'id']);
    }

     public function getDirectionsOfExchanges0()
    {
        return $this->hasMany(DirectionsOfExchange::className(), ['out_signs' => 'id']);
    }

    // === front ====/

    public static function getSignsExchange() {

        $defaultDirection = SiteOptions::getDefaultDirectionOption();
        $dataDirection = DirectionsOfExchange::find()->where(['id' => $defaultDirection])->one();
        $st = '1 ' . self::findOne(['id' => $dataDirection->in_signs])->short_name_currency;
        $st .= ' = ' . $dataDirection->level_exchange . ' ';
        $st .= self::findOne(['id' => $dataDirection->out_signs])->short_name_currency;
        return $st;
    }

    public static function getLeftFormOptions (){

        $defaultDirection = SiteOptions::getDefaultDirectionOption();
        $dataDirection = DirectionsOfExchange::find()->where(['id' => $defaultDirection])->one();
        return self::generateOptions($dataDirection->in_signs);
    }

    public static function getRightFormOptions (){

        $defaultDirection = SiteOptions::getDefaultDirectionOption();
        $dataDirection = DirectionsOfExchange::find()->where(['id' => $defaultDirection])->one();

        return self::generateOptions($dataDirection->out_signs, $dataDirection->in_signs);
    }

    private static function generateOptions($selected, $disabled = null)
    {
        //var_dump($selected, $disabled);
        $dataSings = self::find()->all();
        $option = '';
        foreach ($dataSings as $value) {
            $option .= '<option value="';
            $option .= $value->id. '"';
            if ($value['id'] == $selected) {
                $option .= ' selected="selected"';
            };
            if(isset($disabled) && $value->id == $disabled) {
                $option .= ' disabled="disabled"';
            };
            $option .= '>';
            $option .= $value->full_name_currency;
            $option .= '</option>';
        }
        return $option;
    }

}
