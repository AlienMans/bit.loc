<?php

namespace bitco\exchange\entities;

use Yii;
use yii\db\ActiveRecord;
use bitco\usersModels\entities\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "data_exchange".
 *
 * @property integer $id
 * @property integer $exchange_direction
 * @property string $incoming_amount
 * @property string $outgoing_amount
 * @property string $level_exchange
 * @property string $real_outgoing_amount
 * @property string $real_level_exchange
 * @property integer $user_id
 * @property string $user_fullname
 * @property string $user_name
 * @property string $user_email
 * @property string $user_phone
 * @property string $user_purse
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $admin_user_id
 *
 * @property DirectionsOfExchange $exchangeDirection
 * @property User $user
 */
class DataExchange extends ActiveRecord
{

    const STATUS_ACTIVE = 20;
    const STATUS_PENDING = 10;
    const STATUS_CLOSED = 0;

    public static function tableName()
    {
        return 'data_exchange';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    public function rules()
    {
        return [
            //[['exchange_direction', 'incoming_amount', 'outgoing_amount', 'level_exchange', 'real_outgoing_amount', 'real_level_exchange', 'user_phone', 'created_at', 'updated_at'], 'required'],
            [['exchange_direction', 'status', 'created_at', 'updated_at'], 'integer'],
            [['incoming_amount', 'outgoing_amount', 'level_exchange', 'real_outgoing_amount', 'real_level_exchange', 'user_id', 'admin_user_id'], 'safe'],
            [['user_fullname', 'user_name', 'user_email', 'user_phone', 'user_purse'], 'string', 'max' => 255],
            [['exchange_direction'], 'exist', 'skipOnError' => true, 'targetClass' => DirectionsOfExchange::className(), 'targetAttribute' => ['exchange_direction' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exchange_direction' => 'Направление обмена',
            'incoming_amount' => 'Входящая сумма',
            'outgoing_amount' => 'Выходная сумма',
            'level_exchange' => 'Уровень обмена',
            'real_outgoing_amount' => 'Реальная полученная сумма ',
            'real_level_exchange' => 'Реальный уровень обмена(считать на калькуляторе)',
            'user_id' => 'User ID',
            'user_fullname' => 'User Fullname',
            'user_name' => 'User Name',
            'user_email' => 'User Email',
            'user_phone' => 'User Phone',
            'user_purse' => 'User Purse',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'admin_user_id' => 'Admin User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchangeDirection()
    {
        return $this->hasOne(DirectionsOfExchange::className(), ['id' => 'exchange_direction']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
