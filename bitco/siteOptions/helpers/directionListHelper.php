<?php

namespace bitco\siteOptions\helpers;
use yii\helpers\ArrayHelper;
use bitco\exchange\entities\DirectionsOfExchange;


class directionListHelper
{
    public static function DirectionList () {

       $direction_id =  DirectionsOfExchange::find()->all();
       $array = [];
       for ($i = 0; $i < count($direction_id); $i++) {
            if($direction_id[$i]->isActive()) {
                $array[$i+1] = $direction_id[$i]->inSigns->full_name_currency . ' - ' . $direction_id[$i]->outSigns->full_name_currency;
             }
        }
       return $array;
    }

    public static function DirectionLabel($id) {

        $direction_id =  DirectionsOfExchange::find()->where(['id' => $id])->one();
        $label = $direction_id->inSigns->full_name_currency . ' - ' . $direction_id->outSigns->full_name_currency;
        return $label;
    }

}