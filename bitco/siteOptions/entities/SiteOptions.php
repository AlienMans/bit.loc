<?php

namespace bitco\siteOptions\entities;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "site_options".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $value
 */
class SiteOptions extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'value'], 'required'],
            [['name', 'description'], 'string', 'max' => 255],
            [['value'],  'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя опции',
            'description' => 'Описание настройки',
            'value' => 'Значение настройки',
        ];
    }

    public static function getDefaultDirectionOption () {

        return self::find()->where(['name' => 'defaultDirection'])->one()->value;
    }
}
