<?php

namespace bitco\exchange\entities;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use bitco\exchange\entities\DataExchange;

/**
 * DataExchangeSearch represents the model behind the search form about `bitco\exchange\entities\DataExchange`.
 */
class DataExchangeSearch extends DataExchange
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'exchange_direction', 'incoming_amount', 'outgoing_amount', 'level_exchange', 'real_outgoing_amount', 'real_level_exchange', 'user_id', 'status', 'created_at', 'updated_at', 'admin_user_id'], 'integer'],
            [['user_fullname', 'user_name', 'user_email', 'user_phone', 'user_purse'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataExchange::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'exchange_direction' => $this->exchange_direction,
            'incoming_amount' => $this->incoming_amount,
            'outgoing_amount' => $this->outgoing_amount,
            'level_exchange' => $this->level_exchange,
            'real_outgoing_amount' => $this->real_outgoing_amount,
            'real_level_exchange' => $this->real_level_exchange,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'admin_user_id' => $this->admin_user_id,
        ]);

        $query->andFilterWhere(['like', 'user_fullname', $this->user_fullname])
            ->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'user_email', $this->user_email])
            ->andFilterWhere(['like', 'user_phone', $this->user_phone])
            ->andFilterWhere(['like', 'user_purse', $this->user_purse]);

        return $dataProvider;
    }
}
