<?php

namespace bitco\exchange\entities;

/**
 * This is the ActiveQuery class for [[SignsCurrency]].
 *
 * @see SignsCurrency
 */
class SignsCurrencyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SignsCurrency[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SignsCurrency|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
