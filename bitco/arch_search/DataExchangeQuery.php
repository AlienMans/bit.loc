<?php

namespace bitco\exchange\entities;

/**
 * This is the ActiveQuery class for [[DataExchange]].
 *
 * @see DataExchange
 */
class DataExchangeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return DataExchange[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DataExchange|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
