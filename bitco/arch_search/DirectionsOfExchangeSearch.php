<?php

namespace bitco\exchange\entities;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use bitco\exchange\entities\DirectionsOfExchange;

/**
 * DirectionsOfExchangeSearch represents the model behind the search form about `bitco\exchange\entities\DirectionsOfExchange`.
 */
class DirectionsOfExchangeSearch extends DirectionsOfExchange
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'in_signs', 'in_unit', 'out_signs', 'default_unit', 'level_exchange', 'status', 'updated_at'], 'integer'],
            [['slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DirectionsOfExchange::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'in_signs' => $this->in_signs,
            'in_unit' => $this->in_unit,
            'out_signs' => $this->out_signs,
            'default_unit' => $this->default_unit,
            'level_exchange' => $this->level_exchange,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
