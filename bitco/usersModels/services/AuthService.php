<?php

namespace bitco\usersModels\services;

use bitco\usersModels\entities\User;
use bitco\usersModels\forms\auth\LoginForm;
use bitco\usersModels\repositories\UserRepository;

class AuthService
{
    private $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function auth(LoginForm $form)
    {
        $user = $this->users->findByUsernameOrEmail($form->username);
        if (!$user || !$user->isActive() || !$user->validatePassword($form->password)) {
            throw new \DomainException('Направильный логин или пароль.');
        }
        return $user;
    }
}