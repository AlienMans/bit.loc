<?php
/**
 * Created by PhpStorm.
 * User: AlienMans
 * Date: 01.02.2018
 * Time: 10:07
 */

namespace bitco\usersModels;

use bitco\usersModels\entities\User;
use yii\web\IdentityInterface;


interface IdentityUserInterface
{
    /**
     * @return User
     */
    public function getCurrentUser();

}