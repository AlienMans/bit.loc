<?php
namespace bitco\usersModels\forms\auth;

use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    public function rules()
    {
        return [
            // username and password are both required
            [['username'], 'required', 'message' => 'Введите имя/email пользователя'],
            [['password'], 'required', 'message' => 'Введите пароль'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
        ];
    }


}
