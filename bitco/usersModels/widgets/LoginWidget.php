<?php

namespace bitco\usersModels\widgets;

use yii;
use yii\base\Widget;
use yii\helpers\Html;
use bitco\usersModels\forms\auth\LoginForm;

class LoginWidget extends Widget
{
    private $visible = true;

    public function run()
    {
        if ($this->visible) {
            $user = new LoginForm;
            if ($user->load(Yii::$app->request->post()) && $user->login()) {
                Yii::$app->getResponse()->refresh();
                return '';
            } else {
                return $this->render('loginWidget', [
                    'user' => $user,
                ]);
            }
        }
    }

    public function goHome(){
        Yii::$app->getResponse()->refresh();

    }
}