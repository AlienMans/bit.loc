<?php
/**
 * Created by PhpStorm.
 * User: AlienMans
 * Date: 03.01.2018
 * Time: 9:54
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="panel panel-default">
    <div class="panel-heading">Login</div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($user, 'username', [
            'inputOptions' => [

            ],
        ])->label(false);
        ?>
        <?= $form->field($user, 'password', [
            'inputOptions' => [

            ],
        ])->passwordInput()->label(false);
        ?>
        <div style="color:#999;margin:1em 0">
            If you forgot your password you can <?= Html::a('reset it', ['/site/request-password-reset']) ?>.
        </div>
        <div class="form-group">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            <?= Html::a('Signup', ['/site/signup']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>