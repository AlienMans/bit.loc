<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app',
            'cookieValidationKey' => $params['cookieValidationKey'],
            'baseUrl' => '/backend',
        ],
	'formatter' => [
		'class' => 'yii\i18n\Formatter',
		'decimalSeparator' => '.',
                'locale' => 'ru-RU',
       ],
        'user' => [
            'identityClass' => 'bitco\usersModels\IdentityUsers',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true],
            'loginUrl' => ['site/login'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => '_session',
            'cookieParams' => [
                'domain' => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'backendUrlManager' => require __DIR__ . '/urlManager.php',
        'frontendUrlManager' => require __DIR__ . '/../../frontend/config/urlManager.php',
        'urlManager' => function () {
            return Yii::$app->get('backendUrlManager');
        },

    ],
    'params' => $params,

    'as access' => [
      'class' => 'yii\filters\AccessControl',
      'except' => ['site/login', 'site/logout', 'site/error', 'debug'],
      'rules' => [
          [
              'allow' => true,
              'roles' => ['admin'],
          ],
      ],

    ],
];
