<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model bitco\exchange\entities\SignsCurrency */

$this->title = 'Create Signs Currency';
$this->params['breadcrumbs'][] = ['label' => 'Signs Currencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="signs-currency-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
