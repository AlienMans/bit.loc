<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список валют';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="signs-currency-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php // echo Html::a('Create Signs Currency', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'full_name_currency',
            'short_name_currency',
            [
             'attribute' => 'created_at',
             'format' => 'datetime'
            ],
            'href_icon',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
