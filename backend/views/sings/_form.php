<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model bitco\exchange\entities\SignsCurrency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="signs-currency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'full_name_currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_name_currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'href_icon')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
