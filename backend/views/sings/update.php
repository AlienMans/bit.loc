<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model bitco\exchange\entities\SignsCurrency */

$this->title = 'Update Signs Currency: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Signs Currencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="signs-currency-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
