<?php

use yii\helpers\Html;
use yii\grid\GridView;
use bitco\exchange\helpers\DataExchangeStatusHelper;
use bitco\siteOptions\helpers\directionListHelper;
use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $models bitco\exchange\forms\ExchangeCreateForm */


$this->title = 'Список заказов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-exchange-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php // echo Html::a('Create Data Exchange', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php foreach ($models as $value) { ?>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collpse<?php echo $value->id; ?>">
                        <div class="row">
                            <div class="col-md-1">Заказ № <?= $value->id ?></div>
                            <div class="col-md-2"><?= $value->user_id ? $value->user_name : 'Гость' ?></div>
                            <div class="col-md-5">обмен: <?=
                                $value->incoming_amount . ' ' . $value->exchangeDirection->inSigns->short_name_currency . ' = '
                                . $value->outgoing_amount . ' ' . $value->exchangeDirection->outSigns->short_name_currency
                                . ' по курсу ' . $value->level_exchange
                                ?>
                            </div>
                            <div class="col-md-1"><?php echo DataExchangeStatusHelper::statusLabel($value->status);  ?></div>
                            <div class="col-md-2">Изменено: <?= \Yii::$app->formatter->asDatetime($value->updated_at); ?></div>
                            <div class="col-md-1"><?= Html::a('', ['data-exchange/update', 'id' => $value->id], ['class' => 'glyphicon glyphicon-pencil']) ?></div>
                        </div>
                    </a>
                </div>


                <div id="collpse<?php echo $value->id; ?>" class="panel-collapse collapse out">
                    <div class="panel-body"><h4>Контактные данные:</h4>
                        <p><?= $value->user_fullname ?></p>
                        <p><?= $value->user_name ?></p>
                        <p>Телефон: <?= $value->user_phone ?></p>
                        <p>Email: <?= $value->user_email ?></p>
                        <p>Эл. кошелек: <?= $value->user_purse ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

        <?php

        echo LinkPager::widget(['pagination' => $pages])
        ?>

</div>
