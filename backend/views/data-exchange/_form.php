<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use bitco\exchange\helpers\DataExchangeStatusHelper;

/* @var $this yii\web\View */
/* @var $model bitco\exchange\entities\DataExchange */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-exchange-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php //echo $form->field($model, 'exchange_direction')->textInput() ?>

        <?php //echo $form->field($model, 'incoming_amount')->textInput(['maxlength' => true]) ?>

        <?php //echo $form->field($model, 'outgoing_amount')->textInput(['maxlength' => true]) ?>

        <?php //echo $form->field($model, 'level_exchange')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'real_outgoing_amount')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'real_level_exchange')->textInput(['maxlength' => true]) ?>

        <?php //echo $form->field($model, 'user_id')->textInput() ?>

        <?php //echo  $form->field($model, 'user_fullname')->textInput(['maxlength' => true]) ?>

        <?//= $form->field($model, 'user_name')->textInput(['maxlength' => true]) ?>

        <?//= $form->field($model, 'user_email')->textInput(['maxlength' => true]) ?>

        <?//= $form->field($model, 'user_phone')->textInput(['maxlength' => true]) ?>

        <?//= $form->field($model, 'user_purse')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList(DataExchangeStatusHelper::statusList()) ?>

        <?//= $form->field($model, 'created_at')->textInput() ?>

        <?//= $form->field($model, 'updated_at')->textInput() ?>

        <?php //echo $form->field($model, 'admin_user_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
