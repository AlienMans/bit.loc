<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use bitco\exchange\helpers\DataExchangeStatusHelper;
use bitco\siteOptions\helpers\directionListHelper;

/* @var $this yii\web\View */
/* @var $model bitco\exchange\entities\DataExchange */

$this->title = 'Заказ №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Список заказов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-exchange-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('На список', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'exchange_direction',
                'value' => function ($model) {
                    return directionListHelper::DirectionLabel($model->exchange_direction);
                }
            ],
            'incoming_amount',
            'outgoing_amount',
            'level_exchange',
            'real_outgoing_amount',
            'real_level_exchange',
            'user_id',
            'user_fullname',
            'user_name',
            'user_email:email',
            'user_phone',
            'user_purse',
            [
                'attribute' => 'status',
                'filter' => DataExchangeStatusHelper::statusList(),
                'value' => function ($model) {
                    return DataExchangeStatusHelper::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' =>  'created_at',
                'format' => 'datetime',
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'datetime',
            ],
            //'admin_user_id',
        ],
    ]) ?>

</div>
