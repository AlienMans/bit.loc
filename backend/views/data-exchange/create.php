<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model bitco\exchange\entities\DataExchange */

$this->title = 'Create Data Exchange';
$this->params['breadcrumbs'][] = ['label' => 'Data Exchanges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-exchange-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
