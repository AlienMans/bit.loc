<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use bitco\siteOptions\helpers\directionListHelper;

/* @var $this yii\web\View */
/* @var $model \bitco\siteOptions\entities\SiteOptions */
/* @var $form yii\widgets\ActiveForm */

?>
<?php
//var_dump(directionListHelper::DirectionList());
?>

<div class="site-options-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value')->dropDownList(directionListHelper::DirectionList()); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
