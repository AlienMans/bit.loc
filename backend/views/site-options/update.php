<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \bitco\siteOptions\entities\SiteOptions */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Опции сайта', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->description, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="site-options-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
