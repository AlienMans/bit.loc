<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SiteOptions */

$this->title = 'Create Site Options';
$this->params['breadcrumbs'][] = ['label' => 'Site Options', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-options-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
