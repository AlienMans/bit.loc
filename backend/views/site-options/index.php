<?php

use yii\helpers\Html;
use yii\grid\GridView;
use bitco\siteOptions\helpers\directionListHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Опции сайта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-options-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>-->
<!--        --><?//= Html::a('Create Site Options', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'name',
            'description',
            [
                    'attribute' => 'value',
                    'value' => function ($model) {
                        return directionListHelper::DirectionLabel($model->value);
                    },
                    'format' => 'raw',

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            $url);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
