<?php

use yii\helpers\Html;
use yii\grid\GridView;
use bitco\exchange\helpers\DirectionStatusHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Направления обмена';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directions-of-exchange-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php // echo Html::a('Create Directions Of Exchange', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => 'Направление',
                        'attribute' => 'in_signs',
                        'value' => function ($model) {
                            return $model->getNamedDirection($model->id);
                        }
                    ],
                    //'in_signs',
                    //'in_unit',
                    //'out_signs',
                    [
                        'label' => 'По умолчанию',
                        'attribute' => 'default_unit',
                    ],
                    [
                         'attribute' => 'level_exchange',
                         'format' => 'raw',
                    ],
                    [
                        'attribute' => 'status',
                        'filter' => DirectionStatusHelper::statusList(),
                        'value' => function ($model) {
                            return DirectionStatusHelper::statusLabel($model->status);
                        },
                        'format' => 'raw',
                    ],

                    [
                        'attribute' => 'updated_at',
                        'format' => 'datetime',
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update}',
                        'buttons' => [
                            'update' => function ($url,$model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                    $url);
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
