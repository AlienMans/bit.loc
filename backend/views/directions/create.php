<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\directionsOfExchange */

$this->title = 'Create Directions Of Exchange';
$this->params['breadcrumbs'][] = ['label' => 'Directions Of Exchanges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directions-of-exchange-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
