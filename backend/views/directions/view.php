<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use bitco\siteOptions\helpers\directionListHelper;
use bitco\exchange\helpers\DirectionStatusHelper;

/* @var $this yii\web\View */
/* @var $model bitco\exchange\entities\DirectionsOfExchange */

$this->title =  directionListHelper::DirectionLabel($model->id);
$this->params['breadcrumbs'][] = ['label' => 'Направление обмена', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directions-of-exchange-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php // echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php // echo Html::a('Delete', ['delete', 'id' => $model->id], [
            //'class' => 'btn btn-danger',
            //'data' => [
            //    'confirm' => 'Are you sure you want to delete this item?',
            //    'method' => 'post',
            //],
        //  ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'in_signs',
            'in_unit',
            //'out_signs',
            'default_unit',
            'level_exchange',
            [
                'attribute' => 'status',
                'filter' => DirectionStatusHelper::statusList(),
                'value' => function ($model) {
                    return DirectionStatusHelper::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'datetime',
            ],
        ],
    ]) ?>

</div>
