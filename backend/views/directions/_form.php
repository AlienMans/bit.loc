<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use bitco\exchange\helpers\DirectionStatusHelper;

/* @var $this yii\web\View */
/* @var $model bitco\exchange\entities\DirectionsOfExchange */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="directions-of-exchange-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php // echo $form->field($model, 'in_signs')->textInput() ?>

    <?php // echo $form->field($model, 'in_unit')->textInput() ?>

    <?php // echo  $form->field($model, 'out_signs')->textInput() ?>

    <?= $form->field($model, 'default_unit')->textInput() ?>

    <?= $form->field($model, 'level_exchange')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(DirectionStatusHelper::statusList()) ?>

    <?php // echo $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
