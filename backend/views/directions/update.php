<?php

use yii\helpers\Html;
use bitco\siteOptions\helpers\directionListHelper;

/* @var $this yii\web\View */
/* @var $model \bitco\exchange\entities\DirectionsOfExchange */

$this->title = 'Изменение: ' . directionListHelper::DirectionLabel($model->id);
$this->params['breadcrumbs'][] = ['label' => 'Направление обмена', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="directions-of-exchange-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
