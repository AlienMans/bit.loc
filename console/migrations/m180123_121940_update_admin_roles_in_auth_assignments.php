<?php

use yii\db\Migration;

/**
 * Class m180123_121940_update_admin_roles_in_auth_assignments
 */
class m180123_121940_update_admin_roles_in_auth_assignments extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
	$sql = "UPDATE `auth_assignments` SET `item_name` = 'admin' WHERE `auth_assignments`.`item_name` = 'user' AND `auth_assignments`.`user_id` = 1";
	$sql2 = "UPDATE `auth_assignments` SET `item_name` = 'admin' WHERE `auth_assignments`.`item_name` = 'user' AND `auth_assignments`.`user_id` = 2";

	Yii::$app->db->createCommand($sql)->execute();
	Yii::$app->db->createCommand($sql2)->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180123_121940_update_table_usersass cannot be reverted.\n";

        return false;
    }

}
