<?php

use yii\db\Migration;

/**
 * Handles the creation of table `directions_of_exchange`.
 */
class m180108_162527_create_directions_of_exchange_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%directions_of_exchange}}', [
            'id' => $this->primaryKey(),
            'in_signs' => $this->integer()->notNull(),
            'in_unit' => $this->smallInteger()->notNull()->defaultValue(1),
            'out_signs' => $this->integer()->notNull(),
            'default_unit' => $this->smallInteger()->notNull()->defaultValue(1),
            'level_exchange' => $this->text()->notNull(),
            'slug' => $this->string()->notNull(),
            'status' => $this->smallInteger()->defaultValue(10),
            'updated_at' => $this->integer()->notNull()
        ]);

        $this->createIndex('{{%idx-directions_of_exchange-in_signs}}', '{{%directions_of_exchange}}', 'in_signs');
        $this->createIndex('{{%idx-directions_of_exchange-out_signs}}', '{{%directions_of_exchange}}', 'out_signs');
        $this->createIndex('{{%idx-directions_of_exchange-slug}}', '{{%directions_of_exchange}}', 'slug');

        $this->addForeignKey('{{%fk-directions_of_exchange-in_signs}}', '{{%directions_of_exchange}}', 'in_signs', '{{%signs_currency}}', 'id');
        $this->addForeignKey('{{%fk-directions_of_exchange-out_signs}}', '{{%directions_of_exchange}}', 'out_signs', '{{%signs_currency}}', 'id');

        $this->batchInsert('{{%directions_of_exchange}}', [
            'id',
            'in_signs', 'in_unit', 'out_signs', 'default_unit', 'level_exchange', 'slug',
            'status',
            'updated_at'
        ], [
            [ 1, 
                1, 1, 2, 1, 11169, 'btc-to-usd',
                10,
                time()
            ],
            [ 2, 
                1, 1, 3, 1, 318962.19, 'btc-to-uah',
                10,
                time(),
            ],
            [ 3, 
                2, 1, 1, 100, 0.0000797123, 'usd-to-btc',
                10,
                time(),
            ],
            [ 4, 
                2, 1, 3, 100, 28.9, 'usd-to-uah',
                0,
                time()
            ],
            [ 5, 
                3, 1, 1, 1000, 0.000002742, 'uah-to-btc',
                10,
                time()
            ],
            [ 6, 
                3, 1, 2, 1000, 0.333, 'uah-to-usd',
                0,
                time()
            ],
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%directions_of_exchange}}');
    }
}
