<?php

use yii\db\Migration;

/**
 * Handles the creation of table `signs_currency`.
 */
class m180108_135200_create_signs_currency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%signs_currency}}', [
            'id' => $this->primaryKey(),
            'full_name_currency' => $this->string()->notNull(),
            'short_name_currency' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'href_icon' => $this->string()->defaultValue(''),
        ]);

         $this->batchInsert('{{%signs_currency}}', [
            'id',
            'full_name_currency',
            'short_name_currency',
            'created_at',
            'href_icon',
        ], [
            [
                1,
                'Bitcoin BTC',
                'BTC',
                time(),
                '/images/Bitcoin.png'
            ],
            [
                2,
                'Наличные Киев USD',
                'USD',
                time(),
                ''
            ],
            [
                3,
                'Наличные Киев UAH',
                'UAH',
                time(),
                ''
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%signs_currency}}');
    }
}
