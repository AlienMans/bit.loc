<?php

use yii\db\Migration;

/**
 * Handles the creation of table `data_exchange`.
 */
class m180117_084952_create_data_exchange_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%data_exchange}}', [
            'id' => $this->primaryKey(),
            'exchange_direction' => $this->integer()->notNull(),
            'incoming_amount' => $this->text()->notNull(),
            'outgoing_amount' => $this->text()->notNull(),
            'level_exchange' => $this->text()->notNull(),
            'real_outgoing_amount' => $this->text()->notNull(),
            'real_level_exchange' => $this->text()->notNull(),
            'user_id' => $this->integer()->defaultValue(null),
            'user_fullname' => $this->string(),
            'user_name' => $this->string(),
            'user_email' => $this->string()->notNull(),
            'user_phone' => $this->string()->notNull(),
            'user_purse' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(20),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'admin_user_id' => $this->integer(),

        ]);
        $this->createIndex('{{%idx-data_exchange-exchange_direction}}', '{{%data_exchange}}', 'exchange_direction');
        $this->createIndex('{{%idx-data_exchange-user_id}}', '{{%data_exchange}}', 'user_id');

        $this->addForeignKey('{{%fk-data_exchange-exchange_direction}}', '{{%data_exchange}}', 'exchange_direction', '{{%directions_of_exchange}}', 'id');
        $this->addForeignKey('{{%fk-data_exchange-user_id}}', '{{%data_exchange}}', 'user_id', '{{%users}}', 'id');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%data_exchange}}');
    }
}
