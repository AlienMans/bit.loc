<?php

use bitco\usersModels\entities\User;
use yii\db\Migration;

class m130524_201442_init extends Migration
{

    //Администратор по умолчанию
    const ADMIN1_FIRST_NAME = 'Алексей';
    const ADMIN1_LAST_NAME = 'админ';
    const ADMIN1_EMAIL = '1@1.com';
    const ADMIN1_LOGIN = 'administrator';
    const ADMIN1_PASSWORD = '123qwe';

    //Администратор по умолчанию
    const ADMIN2_FIRST_NAME = 'Сергей';
    const ADMIN2_LAST_NAME = 'админ';
    const ADMIN2_EMAIL = 'admin@example.ru';
    const ADMIN2_LOGIN = 'admin';
    const ADMIN2_PASSWORD = 'admin';

    //Модератор по умолчанию
    const MODERATOR_FIRST_NAME = 'Имя_модератора';
    const MODERATOR_LAST_NAME = 'Фамилия_модератора';
    const MODERATOR_EMAIL = 'moderator@example.ru';
    const MODERATOR_LOGIN = 'moderator';
    const MODERATOR_PASSWORD = 'moderator';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'hash_id' => $this->string(), // $this->string()->notNull()
            'username' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string(),
            'email' => $this->string()->notNull()->unique(),
            'email_confirm_token' => $this->string(),
            'auth_key' => $this->string(32)->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'surname' => $this->string(),
            'name' => $this->string(),
            'middle_name' => $this->string(),
            'number_phone' => $this->string(),
            'skype' => $this->string(),
            'wwwsaite' => $this->string(),
            'passport' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-users-hash_id',
            'users',
            'hash_id'
        );
        $this->createIndex(
            'idx-users-username',
            'users',
            'username'
        );
        $this->createIndex(
            'idx-users-email',
            'users',
            'email'
        );
        $this->batchInsert('{{%users}}', [
            'id',
            'username',
            'password_hash',
            'email',
            'auth_key',
            'status',
            'surname',
            'name',
            'created_at',
            'updated_at'
        ], [
            [
                1,
                self::ADMIN1_LOGIN,
                Yii::$app->security->generatePasswordHash(self::ADMIN1_PASSWORD),
                self::ADMIN1_EMAIL,
                Yii::$app->security->generateRandomString(),
                User::STATUS_ACTIVE,
                self::ADMIN1_LAST_NAME,
                self::ADMIN1_FIRST_NAME,
                time(),
                time()
            ],
            [
                2,
                self::ADMIN2_LOGIN,
                Yii::$app->security->generatePasswordHash(self::ADMIN2_PASSWORD),
                self::ADMIN2_EMAIL,
                Yii::$app->security->generateRandomString(),
                User::STATUS_ACTIVE,
                self::ADMIN2_LAST_NAME,
                self::ADMIN2_FIRST_NAME,
                time(),
                time()
            ],
            [
				3,
                self::MODERATOR_LOGIN,
                Yii::$app->security->generatePasswordHash(self::MODERATOR_PASSWORD),
                self::MODERATOR_EMAIL,
                Yii::$app->security->generateRandomString(),
                User::STATUS_ACTIVE,
                self::MODERATOR_LAST_NAME,
                self::MODERATOR_FIRST_NAME,
                time(),
                time()
            ]
        ]);

    }

    

    public function down()
    {
        $this->dropTable('{{%users}}');
    }
}
