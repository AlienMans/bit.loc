<?php

use yii\db\Migration;

/**
 * Handles the creation of table `history_update_exchange`.
 */
class m180108_172024_create_history_update_exchange_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%history_update_exchange}}', [
            'id' => $this->primaryKey(),
            'directions_id' => $this->smallInteger()->notNull(),
            'old_in_unit' => $this->smallInteger()->notNull(),
            'old_out_unit' => $this->smallInteger()->notNull(),
            'old_level_exchange' => $this->text()->notNull(),
            'old_status' => $this->smallInteger(),
            'updated_at' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%history_update_exchange}}');
    }
}
