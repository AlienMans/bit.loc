<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_options`.
 */
class m180124_082903_create_site_options_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%site_options}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'value' =>$this->string()->notNull(),
        ]);
        
        $this->batchInsert('{{%site_options}}', [
            'id', 'name', 'description', 'value'
        ], [
            [ 1, 
                'defaultDirection', 'Направление по умолчанию для виджета обмена', '1'
            ]
        ]);
	
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%site_options}}');
    }
}
